(function () {

    var arr = [],
        sort = document.getElementById('sort'),
        Lin = document.getElementById('lin'),
        Rin = document.getElementById('rin'),
        Lout = document.getElementById('lout'),
        Rout = document.getElementById('rout'),
        start = document.getElementById('start');

    var domUtil = {

        leftIn: function (value) {
            if (value > 10 && value <= 100) {
                var div = this.CreateNum(div, value);
                arr.unshift(value);
                sort.insertBefore(div, sort.firstChild);
            } else {
                alert("请输入10-100间的数值")
            }
        },

        rightIn: function (value) {
            if (value > 10 && value <= 100) {
                var div = this.CreateNum(div, value);
                arr.push(value);
                sort.appendChild(div);
            } else {
                alert("请输入10-100间的数值")
            }
        },

        leftOut: function () {
            if (sort.firstChild != null) {
                arr.shift();
                sort.removeChild(sort.firstChild);
            } else {
                alert('已经没有数据移了')
            }
        },

        rightOut: function () {
            if (sort.lastChild != null) {
                arr.pop();
                sort.removeChild(sort.lastChild);
            } else {
                alert('已经没有数据移了');
            }
        },

        randNum: function () {
            sort.innerHTML = null;
            arr.length = 0;
            for (var i = 0; i < 15; i++) {
                domUtil.rightIn(parseInt(Math.random() * 90 + 10));
            }
        },

        CreateNum: function (el, value) {
            el = document.createElement('div');
            el.style.height = value * 4 + 'px';
            return el;
        }
    };

    function BubbleSort(el) {
        var len = arr.length,
            div = el,
            i = 0,
            j = 0,
            temp,
            clear = null;
        clear = setInterval(run, 15);

        function run() {
            if (i < len) {
                if (j < len - i - 1) {
                    if (arr[j] > arr[j + 1]) {
                        temp = arr[j];
                        arr[j] = arr[j + 1];
                        arr[j + 1] = temp;
                        div[j].style.height = arr[j] * 4 + 'px';
                        div[j + 1].style.height = arr[j + 1] * 4 + 'px';
                    }
                    j++;
                    return;
                } else {
                    j = 0;
                }
                i++;
            } else {
                clearInterval(clear);
            }

        }
    }


    Lin.onclick = function () {
        var value = parseInt(document.getElementById('num').value);
        /^([0-9]{1,2}|100)$/.test(value) ? domUtil.leftIn(value) : alert('请输入正确数值');
    }

    Rin.onclick = function () {
        var value = parseInt(document.getElementById('num').value);
        /^([0-9]{1,2}|100)$/.test(value) ? domUtil.rightIn(value) : alert('请输入正确数值');
    }


    Lout.onclick = domUtil.leftOut;
    Rout.onclick = domUtil.rightOut;
    rand.onclick = domUtil.randNum;

    start.onclick = function () {
        var div = document.getElementById('sort').getElementsByTagName('div');
        BubbleSort(div);
    }


})();