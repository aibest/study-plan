var data = [];
(function () {
    //element元素，type事件类型,handler绑定函数
    function addHandler(element, type, handler) {
        if (element.addEventListener) {
            addHandler = function (element, type, handler) {
                element.addEventListener(type, handler, false);
            };
        } else if (element.attachEvent) {
            addHandler = function (element, type, handler) {
                element.attachEvent("on" + type, handler);
            };
        } else {
            addHandler = function (element, type, handler) {
                element["on" + type] = handler;
            };
        }
        return addHandler(element, type, handler);
    };
    var lin = document.getElementById("left-in"),
        lout = document.getElementById("left-out"),
        rin = document.getElementById("right-in"),
        rout = document.getElementById("right-out"),
        searchText = document.getElementById("search-text"),
        btn = document.getElementById("search"),
        result = document.getElementById("result");

    addHandler(lin, "click", leftIn);
    addHandler(rin, "click", ringhtIn);
    addHandler(lout, "click", leftOut);
    addHandler(rout, "click", ringhOut);
    addHandler(btn, "click", search);
})();
//进方法
function leftIn() {
    var text = document.getElementById('input').value,
        newArray = [],
        temp;
    if (!(temp = text)) {
        return false;
    }//判断输入值不为空
    newArray = text.split(/[^0-9a-zA-Z\u4e00-\u9fa5]+/);//将非数字，字母，中文等字符设定为划分符，将剩余元素组成数组
    newArray.forEach(element => {
        var result = document.getElementById("result");
        var str = "<span class='select'>" + element + "</span>";
        var message = "<div>" + str + "</div>" + result.innerHTML;
        result.innerHTML = message;
    });
};
function ringhtIn() {
    var text = document.getElementById('input').value,
        newArray = [],
        temp;
    if (!(temp = text)) {
        return false;
    }//判断输入值不为空
    newArray = text.split(/[^0-9a-zA-Z\u4e00-\u9fa5]+/);//将非数字，字母，中文等字符设定为划分符，将剩余元素组成数组
    newArray.forEach(element => {
        var result = document.getElementById("result");
        var str = "<span class='select'>" + element + "</span>";
        result.innerHTML += "<div>" + str + "</div>";
    });
};
//出方法
function leftOut() {
    var result = document.getElementById("result");
    oldEle = result.firstChild;
    console.log(oldEle);
    if (!oldEle) {
        alert("队列空了");
    } else {
        result.removeChild(oldEle);
    }
}
function ringhOut() {
    var result = document.getElementById("result");
    oldEle = result.lastChild;
    console.log(oldEle);
    if (!oldEle) {
        alert("队列空了");
    } else {
        result.removeChild(oldEle);
    }
}
//查询方法
function search() {
    var text = document.getElementById("search-text").value,
        result = document.getElementById('result');
        console.log(result.innerHTML);
        data = result.innerHTML.replace(/<.+?>/gim,' ').split(/[^0-9a-zA-Z\u4e00-\u9fa5]+/);
        //去掉class,div,span,select后找出文本
    result.innerHTML =
        data.map(function (d) {
            if (text != null && text.length > 0) {
                d = d.replace(new RegExp(text, "g"), "<span class='select-text'>" + text + "</span>");
            }
            return "<div>" + d + "</div>";
        }).join('');
}