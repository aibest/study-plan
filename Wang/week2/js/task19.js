(function () {
    //element元素，type事件类型,handler绑定函数
    function addHandler(element, type, handler) {
        if (element.addEventListener) {
            addHandler = function (element, type, handler) {
                element.addEventListener(type, handler, false);
            };
        } else if (element.attachEvent) {
            addHandler = function (element, type, handler) {
                element.attachEvent("on" + type, handler);
            };
        } else {
            addHandler = function (element, type, handler) {
                element["on" + type] = handler;
            };
        }
        return addHandler(element, type, handler);
    };

    //初始数据或者打乱数据
    function init(queue, lin) {
        var input = document.querySelector("input");
        queue.innerHTML = "";//确保ul中内容为空，所泽随机数据时会数据冗余
        for (var i = 0; i < 10; i++) {
            input.value = Math.floor(Math.random() * 90) + 10;
            lin.click();
        }
    };

    //为按钮绑定事件    
    var btns = document.querySelectorAll("button"),
        lin = btns[0],
        rin = btns[1],
        lout = btns[2],
        rout = btns[3],
        messBtn = btns[4],
        bubbleBtn = btns[5],
        selectionBtn = btns[6],
        insertionBtn = btns[7],
        queue = document.querySelector("ul");

    addHandler(lin, "click", leftIn);
    addHandler(rin, "click", rightIn);
    addHandler(lout, "click", leftOut);
    addHandler(rout, "click", rightOut);
    addHandler(messBtn, "click", function () {
        init(queue, lin);
    });
    addHandler(bubbleBtn, "click", function () {
        bubbleSort(queue);
    });
    addHandler(selectionBtn, "click", function () {
        slectionSort(queue);
    });
    addHandler(insertionBtn, "click", function () {
        insertionSort(queue);
    });

    init(queue, lin);
})();//按钮绑定事件

//进方法
function leftIn() {
    var queue = document.querySelector("ul"),
        input = document.querySelector("input"),
        newEle = document.createElement("li"),
        oldEle = queue.querySelectorAll("li")[0],
        temp;

    if (!(temp = transValue(input))) {
        return false;
    }//判断输入值不为空
    newEle.style.height = (temp * 5) + "px";
    newEle.style.width = 20 + "px";
    if (queueLength(queue) >= 60) {
        alert("队列满了");
    } else if (!oldEle) {
        queue.appendChild(newEle);
    } else {
        queue.insertBefore(newEle, oldEle);//将newEle插入到oldEle前
    }
};

function rightIn() {
    var newEle = document.createElement("li"),
        queue = document.querySelector("ul"),
        input = document.querySelector("input"),
        temp;

    if (!(temp = transValue(input))) {
        return false;
    }
    newEle.style.height = temp + "px";
    //新加元素值化为高
    if (queueLength(queue) >= 60) {
        alert("队列满了");
    } else {
        queue.appendChild(newEle);
    }//判断队列大小，是否继续添加
};

//出方法
function leftOut() {
    var queue = document.querySelector("ul"),
        oldEle = queue.firstChild;

    if (!oldEle) {
        alert("队列空了");
    } else {
        alert(oldEle.offsetHeight / 5);
        queue.removeChild(oldEle);
    }
};

function rightOut() {
    var queue = document.querySelector("ul"),
        oldEle = queue.lastChild;//选中子元素（方便右边出）

    if (!oldEle) {
        alert("队列空了");
    } else {
        alert(oldEle.offsetHeight / 5);//输出像素高度
        queue.removeChild(oldEle);
    }
};

//长度方法
function queueLength(queue) {
    return queue.querySelectorAll("li").length;
};
//input中输入值的规范
function transValue(input) {
    var result = parseInt(input.value.replace(/\D/g, ""), 10);//替换非数字为空
    if (result > 100 || result < 10) {
        input.value = "必须为10-100的整数！";
        return false;
    }
    return result;
};
//元素高度交换实现逻辑,用于排序
function swap(ele1, ele2) {
    var temp = ele1.offsetHeight;

    ele1.offsetHeight = ele2.offsetHeight;
    ele1.style.height = ele2.offsetHeight + "px";
    ele2.offsetHeight = temp;
    ele2.style.height = temp + "px";

    // 如果只是相邻元素swap，可以使用下面这个方法直接交换dom元素
    // 但是考虑到非冒泡排序算法使用swap时不一定是交换相邻元素(比
    // 如插入排序)，所以使用交换高度的方法。注意ele.style.height
    // 和ele.offsetHeight都需要互换

    // ele1.parentNode.insertBefore(ele2, ele1);
};

function bubbleSort(queue) {
    var eles = queue.querySelectorAll("li"),
        len = eles.length, i, j = 0, delay = 50, timer;

    i = len - 1;
    timer = setInterval(function () {
        if (i < 1) {
            clearInterval(timer);
        }//清除计时器
        if (j == i) {
            --i;
            j = 0;
        }//从第一个未排序的元素开始排序
        if (eles[j].offsetHeight > eles[j + 1].offsetHeight) {
            swap(eles[j], eles[j + 1]);
        }//冒泡运用
        ++j;
    }, delay);
};
//选择排序
function slectionSort(queue) {
    var eles = queue.querySelectorAll("li"),
        len = eles.length, i = 0, j = 1, min = 0, delay = 50, timer;

    timer = setInterval(function () {
        if (i == len - 1) {
            clearInterval(timer);
        }
        if (j == len) {
            swap(eles[i], eles[min]);
            ++i;
            min = i;
            j = i + 1;
        }
        if (eles[j] && eles[j].offsetHeight < eles[min].offsetHeight) {
            min = j;
        }
        ++j;
    }, delay);
};

/**插入排序
 * 用两个变量控制内外循环
 */
function insertionSort(queue) {
    var eles = queue.querySelectorAll("li"),
        len = eles.length,
        temp, i = 1, j = 0, timer, delay = 100, outer = true, inner = false;

    timer = setInterval(function () {
        if (outer) {
            if (i == len) {
                clearInterval(timer);
                return;
            }
            if (eles[i].offsetHeight < eles[i - 1].offsetHeight) {
                temp = eles[i].offsetHeight;
                j = i - 1;
                outer = false;
                inner = true;
            } else {
                i++;
            }
        }
        if (inner) {
            if (j < 0 || eles[j].offsetHeight < temp) {
                eles[j + 1].style.height = temp + "px";
                eles[j + 1].offsetHeight = temp;
                i++;
                inner = false;
                outer = true;
            } else {
                eles[j + 1].style.height = eles[j].style.height;
                eles[j + 1].offsetHeight = eles[j].offsetHeight;
                j--;
            }
        }
    }, delay);
};