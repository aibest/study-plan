# study-plan

## 2021.01.22-2021.01.29
### HTML的学习

+ 1 预期目标
	+ 理解HTML的书写规范，熟练掌握常用HTML标签。
+ 2 主要内容
    + 标题 段落 图片 链接 列表 表格 表单等
+ 3 参考资料
    + [W3School](http://www.w3school.com.cn/css/css_intro.asp)
+ 4 推荐书籍
    + 《Head First HTML》


### CSS的学习

+ 1 预期目标
	+ 熟悉CSS语法，理解CSS选择器、盒子模型（框模型）、CSS定位等方面的内容。
+ 2 主要内容
    + CSS选择器 
    + CSS继承 
    + 文本，边框，背景，行高等相关属性 
    + 块状元素，内联元素和内联块状元素 
    + 盒子模型
    + CSS浮动
    + CSS定位
    + 常见布局方式
+ 3 参考资料
    + [W3School](http://www.w3school.com.cn/css/css_intro.asp)
    + [王福朋 - 博客园 CSS知多少](http://www.cnblogs.com/wangfupeng1988/p/4276321.html)
    + [学习CSS布局](http://zh.learnlayout.com/display.html)
+ 4 推荐书籍
    + 《CSS权威指南》

### 任务
+ （必做）百度前端学院【任务3】、【任务4】、【任务7】、【任务10】http://ife.baidu.com/2016/task/all
+ 任务3
	+ 说明：http://ife.baidu.com/2016/task/detail?taskId=3
	+ 参考：https://github.com/yuanguangxin/Baidu_ife/tree/master/mission1_3
+ 任务4
	+ 说明：http://ife.baidu.com/2016/task/detail?taskId=4
	+ 参考：https://github.com/BUPTSmallHuoban/BUPTSmallHuoban.github.io/tree/master/task4
+ 任务7
	+ 说明：http://ife.baidu.com/2016/task/detail?taskId=7
	+ 参考：https://github.com/chenBuJuan/IFE-FirstStage-Task7
+ 任务10
	+ 说明：http://ife.baidu.com/2016/task/detail?taskId=10
	+ 参考：https://github.com/alkalixin/IFETask/tree/master/task_10
	
### 面试题
+ position 几个属性的作用
+ 浮动与清除浮动
+ 哪些元素会生成 BFC
+ box-sizing是什么
+ px，em，rem 的区别
+ 流式布局与响应式布局的区别
+ CSS隐藏元素的几种方式及区别
+ 各种页面常见布局https://juejin.cn/post/6844903574929932301

## 2021.02.01-2021.02.07
### Javascript的学习
####  参考书籍

* JavaScript高级程序设计
    * 1-11章，其中第6章，9章暂时不用看

* 你不知道的JavaScript
    


####  相关文章（参考）
* [给JavaScript初学者的24条最佳实践][2] 
* [图解Javascript上下文与作用域][3]
* [深入理解javascript原型和闭包系列][4]

####  JavaScript基础主要内容
* 5种基本数据类型、1种复杂数据类型、操作符、控制语句、函数等

* JavaScript内置对象及常用方法

* 常见DOM树操作大全

* ECMAScript，DOM,BOM

* 定时器和焦点图


####  必做练习

* [任务十四：零基础JavaScript编码（二）][5]

* [任务十五：零基础JavaScript编码（三][6]）

* [任务十七：零基础JavaScript编码（五）][8]

* [练习使用JavaScript实现简单的排序算法][9]

* [实践JavaScript数组、字符串相关操作][10]

* [使用JavaScript实现拖拽功能][11]

* 下拉菜单

* Tab选项切换

* 定时器效果（setInterval,setTimeout,clearInterval,clearTimeout,）

* 轮播图

  
  
  
  
  
  
  
  
  
  
  
  


  [1]: https://pan.baidu.com/s/1dF7uO4h
  [2]: http://yanhaijing.com/javascript/2013/12/11/24-JavaScript-best-practices-for-beginners/
  [3]: http://blog.rainy.im/2015/07/04/scope-chain-and-prototype-chain-in-js/
  [4]: http://www.cnblogs.com/wangfupeng1988/p/4001284.html
  [5]: http://ife.baidu.com/2016/task/detail?taskId=14
  [6]: http://ife.baidu.com/2016/task/detail?taskId=15
  [7]: http://ife.baidu.com/2016/task/detail?taskId=16
  [8]: http://ife.baidu.com/2016/task/detail?taskId=17
  [9]: http://ife.baidu.com/2016/task/detail?taskId=19
  [10]: http://ife.baidu.com/2016/task/detail?taskId=20
  [11]: http://ife.baidu.com/2016/task/detail?taskId=21